/*  global _ */

import RoiCalculator from '../lib/roi-calculator'

import scrollTo from '../lib/scroll-to'

export default {
  name: 'app',
  data () {
    return {
      msg: `ROI Calculator`,
      title: `ROI Calculator`,
      address: `1850 Gateway Boulevard Suite 1060 Concord, CA 94520 Tel 925-681-2326 Fax 925-682-2900`,
      roiInfo: `Below is an assessment of the time and cost savings 
        you can expect from an investment in D-Tools based on 
        actual results reported by typical D-Tools customers.`,
      roi: {
        calculated: false,
        submitted: false,
        fields: {},
        results: {}
      }
    }
  },
  methods: {
    onRoiSubmitted (roi) {
      roi.results = {}
      if (roi.submitted) {
        let data = _.mapValues(roi.fields, 'value')
        roi.results = RoiCalculator.calculate(data)
        setTimeout(function () {
          scrollTo('.roi-info')
        }, 500)
      } else {
        scrollTo('.roiCalculatorContainer')
      }
      roi.calculated = roi.submitted && !_.isEmpty(roi.results)
      this.roi = _.cloneDeep(roi)
    }
  }
}
