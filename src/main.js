import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import VeeValidate from 'vee-validate'
import _ from 'lodash'

import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import App from './App/index.vue'
import RoiForm from './components/roi-form/index.vue'
import RoiAnalyst from './components/roi-analyst/index.vue'
import PrintButton from './components/print-button/index.vue'
import AssumptionContent from './components/assumptions-content/index.vue'
import ContactSales from './components/contact-sales/index.vue'
import InputNumberFormatted from './components/input-number-formatted/index.vue'

Vue.use(BootstrapVue)
Vue.use(VeeValidate)
Vue.use(_)

Vue.component(RoiForm.name, RoiForm)
Vue.component(InputNumberFormatted.name, InputNumberFormatted)
Vue.component(RoiAnalyst.name, RoiAnalyst)
Vue.component(PrintButton.name, PrintButton)
Vue.component(AssumptionContent.name, AssumptionContent)
Vue.component(ContactSales.name, ContactSales)

window.vueApp = new Vue({
  el: '#app',
  render: h => h(App)
})
