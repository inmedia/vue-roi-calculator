import $ from 'jquery'

export default function scrollTo (element, options = {offsetTop: 0}) {
  const ele = $(element)
  $('html, body').animate({
    scrollTop: ele.offset().top - Number.parseInt(options.offsetTop, 10) || 0
  }, 800)
}
