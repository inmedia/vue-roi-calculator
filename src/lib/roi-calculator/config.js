/*  global _ */
import accounting from 'accounting-js'

export default {
  proposals: {
    unit: {
      timeSaved (source, result, key, obj) {
        let value = (unformat(source.avgTimeToGenerateProposal) || 0) * 0.5
        saveToResults(result, key, value)
        return value
      },
      costSaved (source, result, key, obj) {
        let associatedValue = _.get(result, 'proposals.unit.timeSaved', 0)
        if (_.isFunction(associatedValue)) {
          return 0
        }
        let value = (unformat(source.avgHourCostPerPersonGenProposal) || 0) * associatedValue
        saveToResults(result, key, value)
        return value
      }
    },
    annual: {
      timeSaved (source, result, key, obj) {
        let associatedValue = _.get(result, 'proposals.unit.timeSaved', 0)
        if (_.isFunction(associatedValue)) {
          return 0
        }
        let value = (unformat(source.estProposalsPerYear) || 0) * associatedValue
        saveToResults(result, key, value)
        return value
      },
      costSaved (source, result, key, obj) {
        let associatedValue = _.get(result, 'proposals.unit.costSaved', 0)
        if (_.isFunction(associatedValue)) {
          return 0
        }
        let value = (unformat(source.estProposalsPerYear) || 0) * associatedValue
        saveToResults(result, key, value)
        return value
      }
    }
  },
  design: {
    unit: {
      timeSaved (source, result, key, obj) {
        let value = 'N/A'
        saveToResults(result, key, value)
        return value
      },
      costSaved (source, result, key, obj) {
        let value = (unformat(source.avgProjectSize) || 0) * 0.7 * 0.6 * 0.01
        saveToResults(result, key, value)
        return value
      }
    },
    annual: {
      timeSaved (source, result, key, obj) {
        let value = 'N/A'
        saveToResults(result, key, value)
        return value
      },
      costSaved (source, result, key, obj) {
        let associatedValue = _.get(result, 'design.unit.costSaved', 0)
        if (_.isFunction(associatedValue)) {
          return 0
        }
        let value = (unformat(source.estProjectsPerYear) || 0) * associatedValue
        saveToResults(result, key, value)
        return value
      }
    }
  },
  drawings: {
    unit: {
      timeSaved (source, result, key, obj) {
        let value = (unformat(source.avgTimeToGenerateDrawingSet) || 0) * 0.3
        saveToResults(result, key, value)
        return value
      },
      costSaved (source, result, key, obj) {
        let associatedValue = _.get(result, 'drawings.unit.timeSaved', 0)
        if (_.isFunction(associatedValue)) {
          return 0
        }
        let value = (unformat(source.avgHourCostPerPersonGenDrawing) || 0) * associatedValue
        saveToResults(result, key, value)
        return value
      }
    },
    annual: {
      timeSaved (source, result, key, obj) {
        let associatedValue = _.get(result, 'drawings.unit.timeSaved', 0)
        if (_.isFunction(associatedValue)) {
          return 0
        }
        let value = (unformat(source.estProjectsPerYear) || 0) *
          (source.percentProjectWhereDrawing || 0) / 100 *
          associatedValue
        saveToResults(result, key, value)
        return value
      },
      costSaved (source, result, key, obj) {
        let associatedValue = _.get(result, 'drawings.annual.timeSaved', 0)
        if (_.isFunction(associatedValue)) {
          return 0
        }
        let value = (unformat(source.avgHourCostPerPersonGenDrawing) || 0) * associatedValue
        saveToResults(result, key, value)
        return value
      }
    }
  },
  installation: {
    unit: {
      timeSaved (source, result, key, obj) {
        let isWorkOrdersForTech = source.isWorkOrdersForTech === 'yes'
        let avgWireLabelsPerProject = unformat(source.avgWireLabelsPerProject) || 0
        let avgProjectSize = unformat(source.avgProjectSize) || 0
        let avgHourCostTechnician = unformat(source.avgHourCostTechnician) || 1

        let value = avgWireLabelsPerProject / 120
        if (!isWorkOrdersForTech) {
          value += ((avgProjectSize * 0.3 * 0.5) / avgHourCostTechnician) * 0.1
        }
        saveToResults(result, key, value)
        return value
      },
      costSaved (source, result, key, obj) {
        let associatedValue = _.get(result, 'installation.unit.timeSaved', 0)
        if (_.isFunction(associatedValue)) {
          return 0
        }
        let value = (unformat(source.avgHourCostTechnician) || 0) * associatedValue
        saveToResults(result, key, value)
        return value
      }
    },
    annual: {
      timeSaved (source, result, key, obj) {
        let associatedValue = _.get(result, 'installation.unit.timeSaved', 0)
        if (_.isFunction(associatedValue)) {
          return 0
        }
        let value = (unformat(source.estProjectsPerYear) || 0) * associatedValue
        saveToResults(result, key, value)
        return value
      },
      costSaved (source, result, key, obj) {
        let associatedValue = _.get(result, 'installation.annual.timeSaved', 0)
        if (_.isFunction(associatedValue)) {
          return 0
        }
        let value = (unformat(source.avgHourCostTechnician) || 0) * associatedValue
        saveToResults(result, key, value)
        return value
      }
    }
  },
  purchasing: {
    unit: {
      timeSaved (source, result, key, obj) {
        let value = (unformat(source.numberOfPOPerProject) || 0) / 60
        saveToResults(result, key, value)
        return value
      },
      costSaved (source, result, key, obj) {
        let associatedValue = _.get(result, 'purchasing.unit.timeSaved', 0)
        if (_.isFunction(associatedValue)) {
          return 0
        }
        let value = 30 * associatedValue
        saveToResults(result, key, value)
        return value
      }
    },
    annual: {
      timeSaved (source, result, key, obj) {
        let associatedValue = _.get(result, 'purchasing.unit.timeSaved', 0)
        if (_.isFunction(associatedValue)) {
          return 0
        }
        let value = (unformat(source.estProjectsPerYear) || 0) * associatedValue
        saveToResults(result, key, value)
        return value
      },
      costSaved (source, result, key, obj) {
        let associatedValue = _.get(result, 'purchasing.annual.timeSaved', 0)
        if (_.isFunction(associatedValue)) {
          return 0
        }
        let value = 30 * associatedValue
        saveToResults(result, key, value)
        return value
      }
    }
  },
  totals: {
    unit: {
      timeSaved (source, result, key, obj) {
        let value = getTotals(result, [
          'proposals.unit.timeSaved',
          'design.unit.timeSaved',
          'drawings.unit.timeSaved',
          'installation.unit.timeSaved',
          'purchasing.unit.timeSaved'
        ])

        saveToResults(result, key, value)
        return value
      },
      costSaved (source, result, key, obj) {
        let value = getTotals(result, [
          'proposals.unit.costSaved',
          'design.unit.costSaved',
          'drawings.unit.costSaved',
          'installation.unit.costSaved',
          'purchasing.unit.costSaved'
        ])

        saveToResults(result, key, value)
        return value
      }
    },
    annual: {
      timeSaved (source, result, key, obj) {
        let value = getTotals(result, [
          'proposals.annual.timeSaved',
          'design.annual.timeSaved',
          'drawings.annual.timeSaved',
          'installation.annual.timeSaved',
          'purchasing.annual.timeSaved'
        ])

        saveToResults(result, key, value)
        return value
      },
      costSaved (source, result, key, obj) {
        let value = getTotals(result, [
          'proposals.annual.costSaved',
          'design.annual.costSaved',
          'drawings.annual.costSaved',
          'installation.annual.costSaved',
          'purchasing.annual.costSaved'
        ])

        saveToResults(result, key, value)
        return value
      }
    }
  },
  analysis: {
    investment: {
      upfront (source, result, key, obj) {
        /*
        Calculation:
        Upfront Amount = $3,000 + ($1,400 *
            (Estimated number of software licenses required - 1)) + $500
            (if company uses QuickBooks) + costOnLicenseRange.

            costOnLicenseRange =
              $2,000 for 1-3 licenses
              $3,900 for 4-9 licenses
              $9,500 for 10-15 licenses
              $18,000 for 15-29 licenses
              $27,000 for 30+ licenses
         */
        let estSoftLicenses = Math.abs(Math.round(unformat(source.estSoftLicenses) || 0))
        let isUsingQuickBooks = source.isUsingQuickBooks === 'yes'
        const costOnLicenseRanges = {
          '2000': [0, 4],
          '3900': [4, 10],
          '9500': [10, 15],
          '18000': [15, 30],
          '27000': [30, 99999999999]
        }
        let costOnLicenseRange = _.findKey(costOnLicenseRanges,
          range => _.inRange(estSoftLicenses, ...range)) ||
          _.findLastKey(costOnLicenseRanges)

        let value = 3000 + (1400 * (estSoftLicenses - 1)) + 
          (isUsingQuickBooks ? 500 : 0) + parseFloat(costOnLicenseRange)

        saveToResults(result, key, value)
        return value
      },
      monthly (source, result, key, obj) {
        const firstUserCharge = 99
        const additionalUserCharge = 49
        const extraPersonCharge = 25

        /*
          Calculation:
          Recurring Amount = $99 + ($39 * (Estimated number of software licenses required - 1)) +
                            (Number of salespeople * $25) +
                             ((Number of field technicians -
                             Estimated number of software licenses) * $25)

        */
        let estSoftLicenses = (unformat(source.estSoftLicenses) || 0)
        let numberOfSalesPerson = (unformat(source.numberOfSalesPerson) || 0)
        let numberOfExtraSalesPerson = Math.max(0, numberOfSalesPerson - estSoftLicenses)

        let numberOfFieldPerson = (unformat(source.numberOfFieldPerson) || 0)
        let numberOfExtraFieldPerson = Math.max(0, numberOfFieldPerson - estSoftLicenses)
        let additionalPersons = Math.max(0, (numberOfFieldPerson + numberOfSalesPerson) - estSoftLicenses)

        let value = firstUserCharge +
          (additionalUserCharge * (estSoftLicenses - 1)) +
          // (additionalPersons * 25) +
          ((extraPersonCharge * numberOfExtraSalesPerson) + (extraPersonCharge * numberOfExtraFieldPerson))
        saveToResults(result, key, value)
        return value
      }
    },
    finalRoi: {
      inMonths (source, result, key, obj) {
        /*
           Calculation:
          (Total upfront amount + (total recurring amount * 12)) / Total cost savings per year) * 12

         */
        let investmentUpfront = _.get(result, 'analysis.investment.upfront', 0)
        let investmentMonthly = _.get(result, 'analysis.investment.monthly', 0)
        let totalsAnnualCostSaved = _.get(result, 'totals.annual.costSaved', 1)
        investmentUpfront = _.isFunction(investmentUpfront) ? 0 : investmentUpfront
        investmentMonthly = _.isFunction(investmentMonthly) ? 0 : investmentMonthly
        totalsAnnualCostSaved = _.isFunction(totalsAnnualCostSaved) ? 1 : totalsAnnualCostSaved
        
        let value = (investmentUpfront + (investmentMonthly * 12)) / totalsAnnualCostSaved * 12
        saveToResults(result, key, value)
        return value
      }
    }
  }
}

let getTotals = function (source, paths) {
  let data = _.at(source, paths) || []
  let values = data.filter(v => !_.isFunction(v)) || []
  return values.reduce((a, v) => (parseFloat(a) || 0) + (parseFloat(v) || 0), 0) || 0
}

let saveToResults = function (object, path, value) {
  _.set(object, path, value)
}

let unformat = value => accounting.unformat(value)
