/*  global _ */

import Flat from 'flat'
import config from './config.js'

export default {
  name: 'roi-calculator',
  calculate: inputs => {
    let flatConfig = Flat.flatten(config)
    let results = _.cloneDeepWith(config, v => _.isFunction(v) ? v : undefined)
    _.mapValues(flatConfig, (calculator, key, obj) => calculator(inputs, results, key, obj))
    return results
  }
}
