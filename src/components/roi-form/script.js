/*  global _ */

import config from './config.json'
import {Validator} from 'vee-validate'
import accounting from 'accounting-js'

Validator.extend('numberHasThousandSeparator', value => {
  let uf = accounting.unformat(value)
  return uf !== '' && uf !== undefined && uf !== null && uf !== 'NaN' && uf !== 'NaN.undefined' && !isNaN(uf)
})
Validator.extend('greaterThanZero', value => {
  return accounting.unformat(value) > 0
})
Validator.extend('greaterThanEqualsOne', value => {
  return accounting.unformat(value) >= 1
})

export default {
  name: 'roi-form',
  data () {
    return {
      instructions: `Please provide the following information, 
          and then press "Calculate" to obtain a return on investment 
          estimate for your company.`,
      printInstruction: `Click here to print the page`,
      requiredNote: `Fields marked with <span class="text-danger">*</span> are mandatory`,
      form: {
        invalid: null,
        invalidMessage: `Seems you have not entered values or correct values. 
          Please enter correct values in the fields marked with red`
      },
      roi: {
        fields: config.fields,
        calculated: false,
        submitted: false
      }

    }
  },
  methods: {
    resetROIForm: function (event) {
      // reset model
      _.forIn(this.roi.fields, item => {
        item.value = item.default
      })

      this.roi.submitted = false
      this.form.invalid = null
      this.$emit('roi-submitted', this.roi)
    },
    submitROI: function (event) {
      this.$validator.validate().then((result) => {
        // valid
        this.form.invalid = null
        if (result) {
          this.roi.submitted = true
          this.form.invalid = false
          this.$emit('roi-submitted', this.roi)
        } else {
          // invalid
          _.forIn(this.$validator.fields.items, item => {
            item.el.setCustomValidity(item.el.getAttribute('aria-invalid') === 'true' ? 'error' : '')
          })
          this.form.invalid = true
        }
      })
    }
  }
}
