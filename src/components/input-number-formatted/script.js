import accounting from 'accounting-js'

export default {
  name: 'input-number-formatted',
  props: ['value'],
  data () {
    return {}
  },
  computed: {
    inputListeners: function () {
      const vm = this
      return Object.assign({},
        this.$listeners,
        {
          input: function (event) {
            vm.$emit('input', event.target.value)
          },
          blur: function (event) {
            let value = formatNumber(event.target.value, event.target)
            vm.$emit('blur', value)
            vm.$emit('update:value', value)
          },
          change: function (event) {
            let value = formatNumber(event.target.value, event.target)
            vm.$emit('change', value)
            vm.$emit('update:value', value)
          },
          focus: function (event) {
            let value = unformatNumber(event.target.value, event.target)
            vm.$emit('update:value', value)
            vm.$emit('focus', value)
          }
        }
      )
    }
  }
}

let formatNumber = (value, elm) => {
  let unformattedValue = accounting.unformat(value)
  let isNumber = !isInvalidNumber(unformattedValue)
  let decimalsFormat = elm.getAttribute('data-decimals') || 0
  let currency = elm.getAttribute('data-currency') || ' '
  let isAutoDecimal = decimalsFormat === 'auto'
  let decimals = decimalsFormat === 0 || isAutoDecimal ? 0 : decimalsFormat
  let decimalValue = 0

  if (isNumber && value !== '') {
    value = unformattedValue
    decimalValue = value % 1 > 0 ? (value + '').replace(/^[^.]+?(\.)/, '$1') : 0
    if (isAutoDecimal) {
      value = Math.trunc(value)
    }
    let formattingOptions = {
      precision: decimals,
      symbol: currency
    }
    value = accounting.formatMoney(value, formattingOptions)
  }
  if (isInvalidNumber(value)) {
    value = ''
  }
  if (isAutoDecimal && value && decimalValue) {
    value = (value || 0) + decimalValue
  }

  return value
}
let unformatNumber = (value, elm) => {
  if (value !== '') {
    value = accounting.unformat(value)
  }
  if (isInvalidNumber(value)) {
    value = ''
  }
  return value
}
let isInvalidNumber = value => value === 'NaN' ||
  value === 'NaN.undefined' ||
  value === '' ||
  value === undefined ||
  value === null ||
  Number.isNaN(value)
