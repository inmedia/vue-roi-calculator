/*  global _ */

import formatter from 'format-number'
import config from './config.json'

export default {
  name: 'roi-analyst',
  props: {
    roi: {
      type: Object
    }
  },
  data () {
    return {
      instructions: `Below is an assessment of the time and cost savings 
        you can expect from an investment in D-Tools based on 
        actual results reported by typical D-Tools customers.`,
      printInstruction: `Click here to print the page`,
      resultHeader: `unit`,
      annualizedHeader: `annual`,
      config: config
    }
  },
  methods: {
    roiFormatter: function (value, keys, formats, options = {noSuffixClass: false}) {
      let detectedFormats = [formats.$global]
      let chosenKeys = []
      _.forEachRight(keys, key => {
        chosenKeys.unshift(key)
        let flattenKey = chosenKeys.join('.')
        detectedFormats.push(formats[flattenKey] || {})
      })

      let finalFormat = _.assign({}, ...detectedFormats)
      if (!options.noSuffixClass && finalFormat.suffix) {
        finalFormat.suffix = `<span class="text-nowrap small text-secondary">${finalFormat.suffix}</span>`
      }
      if (!finalFormat.noFormat) {
        value = formatter(finalFormat)(value || 0)
      }
      return value
    }
  },
  filters: {
    labelify: function (value, source) {
      return source[value] || value || ''
    }
  }
}
