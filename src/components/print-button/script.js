export default {
  name: 'print-button',
  data () {
    return {}
  },
  methods: {
    printWindow (event) {
      window.print()
    }
  }
}
