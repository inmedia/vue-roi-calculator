export default {
  name: 'contact-sales',
  data () {
    return {
      trialUrl: `https://d-tools.com/hosted-free-trial-signup/`,
      quoteUrl: `https://d-tools.com/request-a-quote/`,
      tourUrl: `https://d-tools.com/live-demo-webinar/`,
      contactPhone: `+1-866-386-6571 ext. 1`
    }
  },
  methods: {
  }
}
